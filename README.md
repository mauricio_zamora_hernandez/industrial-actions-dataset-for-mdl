# Industrial Actions Dataset for MDL

> Essential hand-actions and tools and components manipulation for manual assemblies.

## Description ##

This dataset contains frame-separated-videos of an operator performing the manual assembly process of a skateboard.

## Properties ##

Format: .jpg

Number of frames per video and tools involved:

- Seq001: 6042. Wrench, screwdriver.
- Seq002: 2752. Pliers, wrench.
- Seq003: 3108. Screwdriver, wrench.
- Seq004: 8808. Hammer, wrench.
- Seq005: 6884. Wrench, screwdriver.
- Seq006: 5640. Ratchet.
- Seq007: 4917. Wrench, screwdriver.
- Seq008: 3816. Screwdriver, pliers.
- Seq009: 0653. Pliers.
- Seq010: 2145. Pliers.
- Seq011: 1351. Pliers.
- Seq012: 0967. Pliers.
- Seq013: 1908. Hammer.
- Seq014: 0814. Drill.
- Seq015: 1339. Drill.

